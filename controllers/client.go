package controllers

import (
	"context"

	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Client is a proxy interface, so we can generate mocks for it and use it in tests
type Client interface {
	Get(ctx context.Context, key client.ObjectKey, obj client.Object) error
	List(ctx context.Context, list client.ObjectList, opts ...client.ListOption) error

	Create(ctx context.Context, obj client.Object, opts ...client.CreateOption) error
	Delete(ctx context.Context, obj client.Object, opts ...client.DeleteOption) error
	Update(ctx context.Context, obj client.Object, opts ...client.UpdateOption) error
	Patch(ctx context.Context, obj client.Object, patch client.Patch, opts ...client.PatchOption) error
	DeleteAllOf(ctx context.Context, obj client.Object, opts ...client.DeleteAllOfOption) error

	Status() client.StatusWriter

	Scheme() *runtime.Scheme

	RESTMapper() meta.RESTMapper
}

// RunnerReconcilerClient is a proxy struct that wraps the native Client method calls
type RunnerReconcilerClient struct {
	client client.Client
}

// NewRunnerReconcilerClient creates a RunnerReconcilerClient from an already existing client.Client
func NewRunnerReconcilerClient(inner client.Client) *RunnerReconcilerClient {
	return &RunnerReconcilerClient{client: inner}
}

// Get retrieves an obj for the given object key from the Kubernetes Cluster.
// obj must be a struct pointer so that obj can be updated with the response
// returned by the Server.
func (r *RunnerReconcilerClient) Get(ctx context.Context, key client.ObjectKey, obj client.Object) error {
	return r.client.Get(ctx, key, obj)
}

// List retrieves list of objects for a given namespace and list options. On a
// successful call, Items field in the list will be populated with the
// result returned from the server.
func (r *RunnerReconcilerClient) List(ctx context.Context, list client.ObjectList, opts ...client.ListOption) error {
	return r.client.List(ctx, list, opts...)
}

// Create saves the object obj in the Kubernetes cluster.
func (r *RunnerReconcilerClient) Create(ctx context.Context, obj client.Object, opts ...client.CreateOption) error {
	return r.client.Create(ctx, obj, opts...)
}

// Delete deletes the given obj from Kubernetes cluster.
func (r *RunnerReconcilerClient) Delete(ctx context.Context, obj client.Object, opts ...client.DeleteOption) error {
	return r.client.Delete(ctx, obj, opts...)
}

// Update updates the given obj in the Kubernetes cluster. obj must be a
// struct pointer so that obj can be updated with the content returned by the Server.
func (r *RunnerReconcilerClient) Update(ctx context.Context, obj client.Object, opts ...client.UpdateOption) error {
	return r.client.Update(ctx, obj, opts...)
}

// Patch patches the given obj in the Kubernetes cluster. obj must be a
// struct pointer so that obj can be updated with the content returned by the Server.
func (r *RunnerReconcilerClient) Patch(ctx context.Context, obj client.Object, patch client.Patch, opts ...client.PatchOption) error {
	return r.client.Patch(ctx, obj, patch, opts...)
}

// DeleteAllOf deletes all objects of the given type matching the given options.
func (r *RunnerReconcilerClient) DeleteAllOf(ctx context.Context, obj client.Object, opts ...client.DeleteAllOfOption) error {
	return r.client.DeleteAllOf(ctx, obj, opts...)
}

// Status knows how to create a client which can update status subresource
// for kubernetes objects.
func (r *RunnerReconcilerClient) Status() client.StatusWriter {
	return r.client.Status()
}

// Scheme returns the scheme this client is using.
func (r *RunnerReconcilerClient) Scheme() *runtime.Scheme {
	return r.client.Scheme()
}

// RESTMapper returns the rest this client is using.
func (r *RunnerReconcilerClient) RESTMapper() meta.RESTMapper {
	return r.client.RESTMapper()
}
