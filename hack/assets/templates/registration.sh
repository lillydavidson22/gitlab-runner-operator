#!/bin/bash
set -e
MAX_REGISTER_ATTEMPTS=30

setup_proxy() {
  input=$*

  if [[ -v HTTP_PROXY ]] && [[ ! -z HTTP_PROXY ]]; then
    input+=" --env \"HTTP_PROXY=${HTTP_PROXY}\""
    git_http_proxy="git config --global http.proxy $HTTP_PROXY"
  fi

  if [[ -v HTTPS_PROXY && ! -z HTTPS_PROXY ]]; then
    input+=" --env \"HTTPS_PROXY=${HTTPS_PROXY}\""
    git_https_proxy="git config --global https.proxy $HTTPS_PROXY"
  fi

  if [[ -v git_http_proxy ]] && [[ ! -z git_http_proxy ]];
  then
    pre_clone_script=$git_http_proxy
  fi

  if [[ ( -v pre_clone_script && ! -z pre_clone_script )  && \
  ( -v git_https_proxy && ! -z git_https_proxy ) ]];
  then
    pre_clone_script+="; $git_https_proxy"
  fi

  if [[ ! -v pre_clone_script && \
  ( -v git_https_proxy && ! -z git_https_proxy ) ]];
  then
    pre_clone_script="$git_https_proxy"
  fi

  if [[ -v pre_clone_script ]] && [[ ! -z pre_clone_script ]]
  then
    input+=" --pre-clone-script \"$pre_clone_script\""
  fi

  echo $input
}

register_command() {
  command="/entrypoint register --non-interactive"

  if [[ ( -v RUNNER_ENV  && ! -z RUNNER_ENV )  || \
  ( -v RUNNER_PRE_CLONE_SCRIPT  && ! -z RUNNER_PRE_CLONE_SCRIPT  ) ]];
  then
    command=$(setup_proxy $command)
  fi

  echo $command
}

for i in $(seq 1 "${MAX_REGISTER_ATTEMPTS}"); do
    echo "Registration attempt ${i} of ${MAX_REGISTER_ATTEMPTS}"
    exec $(register_command)

    retval=$?

    if [ ${retval} = 0 ]; then
    break
    elif [ ${i} = ${MAX_REGISTER_ATTEMPTS} ]; then
    exit 1
    fi

    sleep 5
done

exit 0
