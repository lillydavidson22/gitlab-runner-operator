#!/usr/bin/env python3

import sys
import semver
import functools

versions = [ver.strip() for ver in sys.stdin.readlines()]
versions.sort(key=functools.cmp_to_key(
    lambda a, b: semver.compare(a[1:], b[1:])), reverse=True)

print(*versions, sep="\n")
